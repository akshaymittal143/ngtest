module.exports = function (config) {
  let configuration = {
    ...
    plugins: [
      ...
      require('karma-chrome-launcher'),
      ...
    ],
    ...
    autoWatch: true,
    browsers: ['Chrome'],
    customLaunchers: {
      ChromeHeadless: {
        base: 'Chrome',
        flags: [
          '--headless',
          '--disable-gpu',
          '--no-sandbox',
          '--remote-debugging-port=9222'
        ]
      }
    },
    singleRun: false
  };
  // when you are on the CI Server
  if (process.env.CI_SERVER) {
    configuration.browsers = ['ChromeHeadless'];
  }
  config.set(configuration);
};